#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MAX 100

//funcion para remplazar los saltos de linea de fgets por caracteres nulos
void remplazar(char linea[],char viejo,char nuevo){
	char nuevoArreglo[MAX]={0};
	strcpy(nuevoArreglo,linea);
	for(int i=0; i<sizeof(nuevoArreglo);i++){
		if(linea[i]==viejo){
			linea[i]=nuevo;
		}
	}
}


int main(int argc, char** argv){
	int valor;
	float promedio;
	int bandera= 1;
	int contador=-1;
	int  max=0;
	int min=0;
	int ini=0;
	int tam=1;
	double sumatoria=0;
	char linea[MAX]={0};
	int acumulador=0;	
	double desvi=0;
	int *array=(int *)malloc(tam*sizeof(int));
	while(bandera == 1){
		
		fgets(linea,MAX,stdin); //variable, tamano arreglo, ingreso por teclado
		remplazar(linea,'\n','\0'); 
		if(strcmp(linea,"x") != 0){ //compara si son iguales     
                        //acumulador=acumulador+1;                      contador=contador-1; //              
		
		valor=atoi(linea);
		array[contador+1]=valor;
		acumulador=acumulador+valor; //0+numero ingresado
		contador++; //incrementa
		if (valor > max){
			max= valor;
		}
		if ( valor<min ||contador==0 ){
			min= valor;
			//printf("%d",min);
		}
	} 
	else{
	bandera=-1;
	} 
	
	} 	
	promedio=((float)acumulador/contador);
	while(ini<contador){
			sumatoria= sumatoria + (array[ini]-promedio)*(array[ini]-promedio);
			ini++;
		}
		desvi= sqrt((double)sumatoria/contador);
		free(array);

	printf("%.2f\t%d\t%d\t%.2f",promedio,max,min,desvi);			
} 
